﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyShootingScript : MonoBehaviour {

    [SerializeField]
    float interval;

    public GameObject shot;
    public int shotCount;
    public Transform[] shootAtLocation; 
    public bool[] startShoot;

    int targetPosition;
    float timer;
    int shotsLeft;

	// Use this for initialization
	void Start () {
	    for (int i = 0; i < startShoot.Length; i++) {
            startShoot[i] = false;
        }

        targetPosition = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (targetPosition < shootAtLocation.Length) {
            if (transform.position == shootAtLocation[targetPosition].position && startShoot[targetPosition] == false) {
				Debug.Log("nice");
                startShoot[targetPosition] = true;
                shotsLeft = shotCount;
                timer = 0f;
            }

            if (startShoot[targetPosition] == true) {
                Shoot();
            }
        }
	}

    void Shoot() {
        timer -= Time.deltaTime;
        if (timer <= 0 && shotsLeft > 0) { 
            timer += interval;
            GameObject newShot = Instantiate(shot, transform.position, Quaternion.identity) as GameObject; 
            shotsLeft--;
        }
        if (shotsLeft <= 0) {
            targetPosition++;
        }
    }
}
