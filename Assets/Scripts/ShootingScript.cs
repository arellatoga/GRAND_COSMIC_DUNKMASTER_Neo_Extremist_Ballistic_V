﻿using UnityEngine;
using System.Collections;

public class ShootingScript : MonoBehaviour {

    public GameObject shotPrefab;

    GameObject shot;

    bool currentShotActive;
    // for dribbling the ball. if greater than 0, bounce ball. else, take damage
    float ballInvulnerability;
    // if <= 0, take damage

    float shotCooldown;

    public float ballInvulnerabilityTime;
    public float shotCooldownTime;

    public float shotStrength;

	[SerializeField]
	AudioSource charge;
	[SerializeField]
	AudioSource pew;

	// Use this for initialization
	void Start () {
        currentShotActive = false;
        ballInvulnerability = 0;
        shotStrength = 1;
        shotCooldown = 0;
	}

	// Update is called once per frame
	void Update () {
        // Decrease ball invulnerability (dribble)
        if (ballInvulnerability > 0) {
            ballInvulnerability -= Time.deltaTime;
        }

        if (shotCooldown > 0) {
            shotCooldown -= Time.deltaTime;
        }

        // Only allow shots if there are no active shots
        if (Input.GetMouseButton(0) && shotCooldown <= 0 && !currentShotActive) {
			if (shotStrength < 8.0f) {
				if (!charge.isPlaying)
					charge.Play();
				shotStrength += Time.deltaTime * 5.0f;
			}
			else if (shotStrength >= 8.0f) {
				if (charge.isPlaying) charge.Stop();
			}
        }

	    if (Input.GetMouseButtonUp(0) && !currentShotActive && shotCooldown <= 0) {
            // Instantiate prefab and give it some horizontal speed
            currentShotActive = true;

			if (charge.isPlaying)
				charge.Stop();
			pew.Play();

            Vector2 shotPos = (Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position).normalized * shotStrength;
            shot = Instantiate(shotPrefab, new Vector2(transform.position.x, transform.position.y) + shotPos, Quaternion.identity) as GameObject;
            shot.GetComponent<ProjectileScript>().MoveTowards(Camera.main.ScreenToWorldPoint(Input.mousePosition), shotPos.magnitude);
            ballInvulnerability = ballInvulnerabilityTime;
            shotStrength = 1;
            shotCooldown = shotCooldownTime;
        }

        // Destroy active shot
        if (Input.GetMouseButtonDown(1)) {
            if (currentShotActive) {
                DestroyShot();
				currentShotActive = false;
            }
        }

        // DRIBBLE DA BALL TO NOT GET HIT
        if (Input.GetKeyDown(KeyCode.Space) && ballInvulnerability <= 0) {
            ballInvulnerability = ballInvulnerabilityTime;
        }
	}

    void OnCollisionEnter2D (Collision2D other) {
        if (other.transform.tag == "Player Projectile" && ballInvulnerability <= 0) {
            // Send message to health component, take damage
            transform.parent.GetComponent<PlayerHealthComponent>().health--;
            shotCooldown = 0;
            DestroyShot();
        }
        else if (other.transform.tag == "Player Projectile" && ballInvulnerability > 0) {
            //other.gameObject.GetComponent<ProjectileScript>().Reflect(other.contacts[0]);
        }
    }

    //step function
    float Decider (float x){
        if(x > -0.5)
        {
          return -0.1f;
        }
        if (x < -0.5)
        {
          return 0.1f;
        }
        else
        {
          return 0;
        }

    }

    void DestroyShot () {
        currentShotActive = false;
        Destroy(shot);
    }

    public float CurrentInvuln() {
        return ballInvulnerability;
    }
}
