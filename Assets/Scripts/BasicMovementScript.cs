﻿using UnityEngine;
using System.Collections;

public class BasicMovementScript : MonoBehaviour {

    public float staticSpeed;
    public bool applyStaticSpeed = true;
    public bool willNormalize = false;

    int yDirPrevious;
    int yDirCurrent;

    // Use this for initialization
    void Start() {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        if (applyStaticSpeed) {
            rb.velocity = new Vector2(0, staticSpeed);
        }
        yDirCurrent = -1;
    }

    void Update() {
        if (willNormalize) {
            Normalize();
        }
    }

    public void SetSpeed(float x, float y) {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(x, y);
    }

    public void AddSpeed(float x, float y) {
        Debug.Log("Adding speed..");
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.velocity += new Vector2(x, y);
    }

    public void Reflect() {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.velocity *= -1.05f;
        //rb.velocity = new Vector2(rb.velocity.x * -1.3f, rb.velocity.y * -1.3f);
    }

    // fix velocity so that if the object goes up, the passive movement speed will not affect it
    void Normalize() {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();

        yDirPrevious = yDirCurrent;
        if (rb.velocity.y > 0) {
            yDirCurrent = 1;
        }
        else if (rb.velocity.y < 0) {
            yDirCurrent = -1;
        }

        if (yDirCurrent != yDirPrevious) {
            rb.velocity += new Vector2(0, staticSpeed);
        }

        Debug.Log(rb.velocity.y);
    }
}
