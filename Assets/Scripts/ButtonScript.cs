﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ButtonScript : MonoBehaviour {

	int sceneNo;

	void Start() {
		sceneNo = SceneManager.GetActiveScene().buildIndex;
	}

	public void PauseResume() {
		Time.timeScale = Time.timeScale == 1 ? 0 : 1;
	}

	public void LoadLevel() {
		Time.timeScale = 1;
		SceneManager.LoadScene(1);
	}

	public void LoadMainMenu() {
		Time.timeScale = 1;
		SceneManager.LoadScene(0);
	}

	public void RestartScene() {
		Time.timeScale = 1;
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	public void QuitApplication () {
		Application.Quit();
	}
}
