﻿using UnityEngine;
using System.Collections;

public class GizmoScript : MonoBehaviour {

    public int order = 0;

    public float gizmoSize;
    void OnDrawGizmos() {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(transform.position, gizmoSize);
    }
}
