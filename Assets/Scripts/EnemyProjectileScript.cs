﻿using UnityEngine;
using System.Collections;
//using System.Object.Math;

public class EnemyProjectileScript : MonoBehaviour {

    // Does the shot travel straight? in a sine fashion? zigzag?
    public enum ShotType {straight, sine, homing, bezier};

    public float lifeTime;

    public ShotType shotType;
    Transform target;

    Vector3 targetVector;
    Vector3 sourceVector;
    Vector3 bezierVector;

    public float speed;

    public float frequency = 3f;
    public float amplitude = 0.5f;
    public float phaseShift;

    public float bendFactor = 0.5f;

    float sineSeed;
    float bezierSeed;
    float distance;

    Vector3 velocity;
    Vector3 toTargetAxis;
    Vector3 normalAxis;
    Vector3 lerpValue;
    
    // if target is above or below
    int targetLocation;

    // NOTES :
    // DO *NOT* USE
    // RIGIDBODY2D

	// Use this for initialization
	void Start () {
        target = GameObject.Find("Hitbox").transform;
        targetVector = target.position;
        sourceVector = transform.position;
        Vector3 curvePosVector = targetVector - sourceVector;
        int mult = targetVector.x > sourceVector.x ? -1 : 1;
        curvePosVector = new Vector3(curvePosVector.y, -curvePosVector.x).normalized * curvePosVector.magnitude * bendFactor/4;
        bezierVector = ((targetVector + sourceVector) / 2);
        bezierVector = bezierVector + (mult * curvePosVector);
        distance = (targetVector - sourceVector).magnitude;

        lerpValue = new Vector3((targetVector.x - transform.position.x) * bendFactor * speed/10, -speed, 0);
        sineSeed = 0;
        bezierSeed = 0;

        toTargetAxis = (targetVector - transform.position).normalized;
        normalAxis = new Vector3(toTargetAxis.y, -toTargetAxis.x, 0);

        velocity = (targetVector - transform.position).normalized * speed;
	}

    // Update is called once per frame
    void Update() {
        targetLocation = targetVector.y > transform.position.y ? 1 : -1;

        if (shotType == ShotType.homing) {
            targetVector = target.position;

            if (transform.position.y > targetVector.y + 0.5f) {
                lerpValue = new Vector3((targetVector.x - transform.position.x) * bendFactor * 5, -speed, 0);
            }
            transform.position += lerpValue * Time.deltaTime;
        }
        else if (shotType == ShotType.straight) {
            transform.position += velocity * Time.deltaTime;
        }
        else if (shotType == ShotType.sine) {
            float rads = (frequency * sineSeed * Mathf.Deg2Rad) + (phaseShift * Mathf.Deg2Rad) + (Mathf.PI / 2);
            Vector3 vel = toTargetAxis * speed / 10 + normalAxis * (amplitude * frequency) / 50 * (float)System.Math.Sin(rads);
            transform.position += vel;
            sineSeed++;
        }
        else if (shotType == ShotType.bezier) {
            if (bezierSeed <= 100) {
                float factor = bezierSeed / 100;
                Vector3 newPos = (Mathf.Pow(1 - factor, 2) * sourceVector) + (2 * (1 - factor) * factor * bezierVector) + (Mathf.Pow(factor, 2) * targetVector);
                lerpValue = newPos - transform.position;
                transform.position = newPos;
                bezierSeed += 1 * Time.deltaTime * speed / distance * 200;
            }
            else {
                transform.position += lerpValue;
            }
        }

        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0) {
            Destroy(gameObject);
        }
	}

	public void Destroy() {
		Destroy(gameObject);
	}
}
