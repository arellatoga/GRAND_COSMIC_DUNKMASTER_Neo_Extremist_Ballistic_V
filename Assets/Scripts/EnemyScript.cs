﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour {
    public enum MovementType { Straight, Lerp };
    //public MovementType initialMoveType;

    [Tooltip("Score given to player when destroyed")]
    public int score;

    [Tooltip("The ship will try to approach these points. Index = 0 is the first point it will try to approach")]
    public Transform[] movementPoints;
    [Tooltip("Bezier curve strength")]
    public Transform[] bezierPoints;
    [Tooltip("How the ship will approach the point at given index")]
    public MovementType[] moveTypes;
    //public GameObject normalizeVelocityParent;
    [Tooltip("Delay at these points. Start point is index = 0. First point ship will approach is index = 1. Size should be movement points array length + 1")]
    public float[] delayAtPoint;

    int targetPoint;
    int bezierPoint;
    public int speed;
    public bool repeatPattern;

    int yDirCurrent;
    int yDirPrevious;

    float t;
    float delay;
    Vector3 sourcePosition; // for Bezier curves. dis da original point

	public int health = 1;

    // Use this for initialization
    void Start () {
        targetPoint = 0;
        bezierPoint = 0;
        t = 0;
        sourcePosition = transform.position;
        delay = delayAtPoint[targetPoint];
	}

	// Update is called once per frame
	void Update () {
        if (delay >= 0) {
            delay -= Time.deltaTime;
//            Debug.Log("here");
        }
        else {
            if (moveTypes[targetPoint] == MovementType.Straight) {
                transform.position = Vector2.MoveTowards(transform.position, movementPoints[targetPoint].position, speed * Time.deltaTime);
            }
            else if (moveTypes[targetPoint] == MovementType.Lerp && t <= 100) {
                float factor = t / 100;
                Vector3 newPos = (Mathf.Pow(1 - factor, 2) * sourcePosition) + (2 * (1 - factor) * factor * bezierPoints[bezierPoint].position) + (Mathf.Pow(factor, 2) * movementPoints[targetPoint].position);
                transform.position = newPos;
                t += 1 * Time.deltaTime * speed * 5;
            }

            if (transform.position == movementPoints[targetPoint].position || t > 100) {
                t = 0;
                sourcePosition = movementPoints[targetPoint].position;
                targetPoint++;
                delay = delayAtPoint[targetPoint];
                if (bezierPoints.Length != 0) {
                    bezierPoint = (bezierPoint + 1) % bezierPoints.Length;
                }
                if (targetPoint == movementPoints.Length) {
                    if (!repeatPattern) {
                        Destroy(transform.parent.gameObject);
                    }
                    else {
                        targetPoint = 0;
                    }
                }
            }
        }
	}

    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Player Projectile") {
            GameManagerScript gs = GameObject.Find("Game Manager").GetComponent<GameManagerScript>();
            gs.AddScore(score);
			GetComponent<AudioSource>().Play();
			health--;
			if (health <= 0) {
				Destroy(gameObject);
			}
        }
    }
}
