﻿using UnityEngine;
using System.Collections;

public class SpriteScript : MonoBehaviour {

	[SerializeField]
	bool facePlayer;

	Transform pos;

	// Use this for initialization
	void Start () {
		if (facePlayer) {
			pos = GameObject.Find("Ship").transform;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (facePlayer) {
			gameObject.transform.LookAt(pos.position);
		}
		else {
			gameObject.transform.LookAt(Camera.main.ScreenToWorldPoint(Input.mousePosition));
		}
	}
}
