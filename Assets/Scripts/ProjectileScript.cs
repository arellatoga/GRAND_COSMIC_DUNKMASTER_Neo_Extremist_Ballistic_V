﻿using UnityEngine;
using System.Collections;

public class ProjectileScript : MonoBehaviour {
    public float speed;
    //public float staticSpeed;

    public float minSpeed;
    public float maxSpeed;

    // 1 if up, -1 if down
    int yDirCurrent = 1;
    int yDirPrevious = 1;

    void Update() {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();

        yDirPrevious = yDirCurrent;
        if (rb.velocity.y > 0) {
            yDirCurrent = 1;
        }
        else if (rb.velocity.y < 0) {
            yDirCurrent = -1;
        }

        if (yDirCurrent != yDirPrevious) {
            //rb.velocity += new Vector2(0, staticSpeed);
        }
            //Debug.Log(rb.velocity.y);
    }

    public void SetInitialSpeed(float x, float y) {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(x * speed, y * speed);
    }

    public void MoveTowards(Vector2 to, float str) {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.velocity = (to - new Vector2(transform.position.x, transform.position.y)).normalized * speed * str;
        //Debug.Log(str);

        /*0
        if (rb.velocity.magnitude < minSpeed) {
            rb.velocity *= minSpeed / rb.velocity.magnitude;
        }
        */
        
        if (rb.velocity.magnitude > maxSpeed) {
            rb.velocity = Vector2.ClampMagnitude(rb.velocity, maxSpeed);
        }
        
        if (rb.velocity.magnitude < minSpeed) {
            //Debug.Log("increasing speed");
            rb.velocity = rb.velocity.normalized * minSpeed; 
        }
        
    }
    
    public void SetSpeed(float x, float y) {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(x, y);
    }
    
    public void AddSpeed(float x, float y) {
        //Debug.Log("Adding speed..");
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.velocity += new Vector2(x, y);
    }

    public void Reflect(ContactPoint2D contact) {
        //Debug.Log("Nice");
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        //rb.velocity *= -2f;
        rb.velocity = Vector2.Reflect(rb.velocity, contact.normal);
        //rb.velocity = new Vector2(-rb.velocity.x, -rb.velocity.y + staticSpeed);
    }
}
