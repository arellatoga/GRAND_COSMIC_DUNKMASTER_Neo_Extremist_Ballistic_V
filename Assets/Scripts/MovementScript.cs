﻿using UnityEngine;
using System.Collections;

public class MovementScript : MonoBehaviour {

    public float speed;
    public float staticSpeed;
    Rigidbody2D rb;
    public float focusDivisor;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(0, staticSpeed);
	}

	// Update is called once per frame
	void FixedUpdate () {
	    if (Input.GetKey(KeyCode.A)) {
            rb.velocity = new Vector2(-speed, rb.velocity.y);
        }
        else if (Input.GetKey(KeyCode.D)) {
            rb.velocity = new Vector2(speed, rb.velocity.y);
        }
        else {
            rb.velocity = new Vector2(0, staticSpeed);
        }

        if (Input.GetKey(KeyCode.W)) {
            rb.velocity = new Vector2(rb.velocity.x, staticSpeed + speed);
        }
        else if (Input.GetKey(KeyCode.S)) {
            rb.velocity = new Vector2(rb.velocity.x, staticSpeed - speed);
        }
        else {
            rb.velocity = new Vector2(rb.velocity.x, staticSpeed);
        }
        if (Input.GetKey(KeyCode.LeftShift)) {
          rb.velocity = new Vector2(rb.velocity.x/focusDivisor,rb.velocity.y/focusDivisor);
        }
	}
}
