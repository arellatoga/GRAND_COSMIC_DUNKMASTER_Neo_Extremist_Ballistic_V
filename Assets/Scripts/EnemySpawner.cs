﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

    public GameObject[] waves;
    public float[] waveSpawnTime;
    int currentWave;

	// Use this for initialization
	void Start () {
        currentWave = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (currentWave < waves.Length) {
            Spawn();
        }
	}

    void Spawn() {
        if (waveSpawnTime[currentWave] <= Time.timeSinceLevelLoad) {
            Instantiate(waves[currentWave], waves[currentWave].transform.position, waves[currentWave].transform.rotation);
            currentWave++;
        }
    }
}
