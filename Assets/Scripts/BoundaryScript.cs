﻿using UnityEngine;
using System.Collections;

public class BoundaryScript : MonoBehaviour {

    public float staticSpeed;
	// Use this for initialization
	void Start () {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(0, staticSpeed);
	}
	
    public void SetSpeed(float newSpeed) {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(0, newSpeed);
    }
}
