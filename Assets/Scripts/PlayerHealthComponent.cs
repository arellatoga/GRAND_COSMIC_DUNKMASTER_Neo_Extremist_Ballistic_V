﻿using UnityEngine;
using System.Collections;

public class PlayerHealthComponent : MonoBehaviour {

    public int health;

	[SerializeField]
	float enemyInvulnerabilityTime;
	float enemyInvulnerability;

	// Use this for initialization
	void Start () {
		enemyInvulnerability = 0;
	}
	
	// Update is called once per frame
	void Update () {
		enemyInvulnerability -= Time.deltaTime;
	    if (health <= 0) {
            //Destroy(gameObject);
        }
	}

    void OnTriggerEnter2D (Collider2D other) {
        //Debug.Log("Hello");
        if ((other.tag == "Enemy" || other.tag == "Enemy Projectile") && enemyInvulnerability <= 0) {
            //Debug.Log("enemy");
            health--;
			enemyInvulnerability = enemyInvulnerabilityTime;
			Destroy(other.gameObject);
        }
    }
}
