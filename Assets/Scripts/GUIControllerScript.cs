﻿using UnityEngine;
using System.Collections;
using System;
﻿using UnityEngine.UI;

public class GUIControllerScript : MonoBehaviour {
    // Shot script
    GameObject shotGUI;
    GameObject shotScript;
    Text shotText;

    // Dribbling
    GameObject dribbleGUI;
    Text dribbleText;

    // Health script
    GameObject healthGUI;
    GameObject ship;
    Text healthText;

    // Score script
    GameObject scoreGUI;
    GameObject scoreScript;
    Text scoreText;

	// Use this for initialization
	void Start () {
		shotGUI = GameObject.Find("ShotPowerUI");
		shotScript = GameObject.Find("Hitbox");

        dribbleGUI = GameObject.Find("DribbleUI");

        healthGUI = GameObject.Find("HealthUI");
        ship = GameObject.Find("Ship");

        scoreGUI = GameObject.Find("ScoreUI");
        scoreScript = GameObject.Find("Game Manager");
	}

	// Update is called once per frame
	void GUIUpdate () {
        // shooting
        ShootingScript Shooting = shotScript.GetComponent<ShootingScript>();
        shotText = shotGUI.GetComponent<Text>();
        //shotText.text = String.Format("{0}",Shooting.shotStrength);
        shotText.text = "Power: " + Shooting.shotStrength;

        // drabble
        //float invuln = Shooting.CurrentInvuln();
        dribbleText = dribbleGUI.GetComponent<Text>();
        if (Shooting.CurrentInvuln() <= 0) {
            dribbleText.text = "Vulnerable to own ball!";   
        }
        else {
            dribbleText.text = "Can bounce!";
        } 

        // health
        healthText = healthGUI.GetComponent<Text>();
        //healthText.text = String.Format("{0}", ship.GetComponent<PlayerHealthComponent>().health);
        healthText.text = "Health: " + ship.GetComponent<PlayerHealthComponent>().health;

        // score
        scoreText = scoreGUI.GetComponent<Text>();
        scoreText.text = "Score: " + scoreScript.GetComponent<GameManagerScript>().CurrentScore();
	}

    void Update() {
        GUIUpdate();

        // Pause
        if (Input.GetKeyDown(KeyCode.Escape)) {
            Time.timeScale = Time.timeScale == 0 ? 1 : 0;
        }
    }
}
