﻿using UnityEngine;
using System.Collections;

public class GameManagerScript : MonoBehaviour {

    public int score;
    GameObject player;

	// Use this for initialization
	void Start () {
		Time.timeScale = 1;
        score = 0;
        player = GameObject.Find("Ship");
	}
	
	// Update is called once per frame
	void Update () {
	    if (player.GetComponent<PlayerHealthComponent>().health <= 0) {
            Debug.Log("Game over");
            GetComponent<AudioSource>().Stop();
            Time.timeScale = 0;
        }

		if (Time.timeSinceLevelLoad >= 93) {
			Time.timeScale = 0;
		}
	}

    public void AddScore(int _score) {
        score += _score;
    }

    public int CurrentScore() {
        return score;
    }
}
